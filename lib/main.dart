import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training16/auth/presentation/pages/SignUp.dart';
import 'package:training16/common/colors.dart';
import 'package:training16/common/theme.dart';

Future<void> main() async {
  await initializeDateFormatting("ru");
  await Supabase.initialize(
    url: 'https://hawusttfcqcomedevycj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imhhd3VzdHRmY3Fjb21lZGV2eWNqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDk3MzQwNDIsImV4cCI6MjAyNTMxMDA0Mn0.FaTVbgSsA1uzIp5miFfUdTH4-G7cnIL9CwGs0rOJ1Hs',
  );

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  bool isLight = true;

  void changeTheme(BuildContext context) {
    isLight = !isLight;
    context.findAncestorStateOfType<_MyAppState>()?.onChangedTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLight) ? lightColors : darkColors;
  }

  ThemeData getThemeData(){
    return (isLight) ? lightTheme : darkTheme;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangedTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      theme: widget.getThemeData(),
      home: const SignUp(),
    );
  }
}

