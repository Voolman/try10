import 'package:flutter/material.dart';
import 'package:training16/common/colors.dart';

var lightColors = LightColorsApp();
var lightTheme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w500,
      color: lightColors.text
    ),
    titleMedium: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: lightColors.subtext
    ),
    labelMedium: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w700,
        color: lightColors.disableTextAccent
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: lightColors.subtext, width: 1),
      borderRadius: BorderRadius.circular(4)
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: lightColors.subtext, width: 1),
        borderRadius: BorderRadius.circular(4)
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: lightColors.accent,
      disabledBackgroundColor: lightColors.disableAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
    )
  ),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    selectedLabelStyle: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 12,
      color: lightColors.accent
    ),
    unselectedLabelStyle: TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 12,
        color: lightColors.subtext
    ),
  )
);

var darkColors = DarkColorsApp();
var darkTheme = ThemeData(
    textTheme: TextTheme(
      titleLarge: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.w500,
          color: darkColors.text
      ),
      titleMedium: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w500,
          color: darkColors.subtext
      ),
      labelMedium: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
          color: darkColors.disableTextAccent
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: darkColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        ),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: darkColors.subtext, width: 1),
            borderRadius: BorderRadius.circular(4)
        )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
          backgroundColor: darkColors.accent,
          disabledBackgroundColor: darkColors.disableAccent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
        )
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      selectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 12,
          color: darkColors.accent
      ),
      unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 12,
          color: darkColors.subtext
      ),
    )
);