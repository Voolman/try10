import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training16/auth/data/models/PlatformModel.dart';
import 'package:training16/auth/data/repository/Supabase.dart';

class ChoosePlatformPresenter {

  Map<PlatformModel, bool> platforms = {};

  void unselectPlatform(PlatformModel platform){
    platforms[platform] = false;
  }

  void selectPlatform(PlatformModel platform){
    platforms[platform] = true;
  }

  List<PlatformModel> getSelectedPlatform(){
    return platforms.keys.where((key) => platforms[key]!).toList();
  }

  Future<void> fetchListPlatforms(
      List<PlatformModel> alreadySelectPlatforms,
      Function(Map<PlatformModel, bool>) onResponse,
      Function(String) onError,
      ) async {
      try{
        await getAllPlatform().then((value) =>
            (List<PlatformModel> response){
          var alreadySelectIdsMap = {};
          for (var model in alreadySelectPlatforms) {
            alreadySelectIdsMap[model.id] = model;
          }

          Map<PlatformModel, bool> data = {};
          for (var platform in response) {
            PlatformModel e;
            bool isAlreadyHas = alreadySelectIdsMap.keys.contains(platform.id);
            if (isAlreadyHas){
              e = alreadySelectIdsMap[platform.id]!;
            }else{
              e = platform;
            }
            data[e] = isAlreadyHas;
          }
          onResponse(data);
          },
        );
      }on AuthException catch(e){
        onError(e.message);
      }
  }
}

