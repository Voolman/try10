import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../main.dart';
import '../data/models/PlatformModel.dart';
import '../data/models/ProfileModel.dart';
import '../data/repository/Supabase.dart';

class SetupProfilePresenter {

  Uint8List? avatar;
  String? avatarUrl;
  String? birthday;

  void setFormattedBirthday(DateTime birthday){
    this.birthday = "${DateFormat("dd MMMM yyyy", "ru").format(birthday)} года";
  }

  bool isValidFIO = true;
  bool isValidPhone = true;
  bool isValidPlatforms = true;

  var isAlreadyExistMetadata = checkExistPlatformsMetadata();

  List<PlatformModel> platforms = [];

  bool isValid(
      String fio,
      String phone
      ){
    isValidFIO = true;
    isValidPhone = true;
    if (fio.isEmpty){
      isValidFIO = false;
      return false;
    }
    if (phone.isEmpty){
      isValidPhone = false;
      return false;
    }
    return birthday != null && platforms.isNotEmpty && isValidPlatforms;
  }

  Future<void> fetchUserData(
      Function(ModelProfile) onExist
      ) async {
    if (isAlreadyExistMetadata) {
      var modelProfile = getModelProfile();
      platforms = modelProfile.platforms;
      avatarUrl = modelProfile.getFullAvatarUrl();
      onExist(modelProfile);
    }
  }

  Future<void> pressPickAvatarFromCamera({required Function() onPick}) async {
    XFile? avatarFile = await ImagePicker().pickImage(
        source: ImageSource.camera
    );
    if (avatarFile != null){
      avatar = await avatarFile.readAsBytes();
      onPick();
    }
  }

  Future<void> pressPickAvatarFromGallery({required Function() onPick}) async {
    XFile? avatarFile = await ImagePicker().pickImage(
        source: ImageSource.gallery
    );
    if (avatarFile != null){
      avatar = await avatarFile.readAsBytes();
      onPick();
    }
  }

  void deleteAvatar() {
    avatar = null;
    avatarUrl = null;
  }

  Future<void> pressConfirm(
      String fullname,
      String phone,
      Function() onConfirm,
      Function(String) onError,
      ) async {
    try{
      await saveUserData(fullname, phone, birthday!);
      platforms = await getAllPlatform();
      await savePlatforms(platforms);
      onConfirm();
    } on AuthException catch(e){
      onError(e.message);
    } on PostgrestException catch(e){
      onError(e.message);
    } on Exception catch(e){
      onError(e.toString());
    }
  }

  void pressChangeTheme(BuildContext context) {
    MyApp.of(context).changeTheme(context);
  }
}
