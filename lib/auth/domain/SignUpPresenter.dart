import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training16/auth/data/repository/Supabase.dart';

Future<void> pressSignUp(
    String email,
    String password,
    Function onResponse,
    Function(String) onError
    ) async {
  try{
    await signUp(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.toString());
  }on Exception catch(e){
    onError(e.toString());
  }
}