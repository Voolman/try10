import 'package:flutter/material.dart';
import 'package:training16/auth/domain/NewPasswordPresenter.dart';
import 'package:training16/common/utils.dart';
import 'package:training16/common/widgets/CustomTextField.dart';

import '../../../common/colors.dart';
import 'LogIn.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});

  @override
  State<NewPassword> createState() => _NewPasswordState();
}
class _NewPasswordState extends State<NewPassword> {
  @override
  Widget build(BuildContext context) {
    TextEditingController password = TextEditingController();
    TextEditingController passwordConfirm = TextEditingController();
    void onChanged(_){}
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Новый пароль',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Введите новый пароль',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Пароль",
                    hint: "**********",
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Повторите пароль",
                    hint: "**********",
                    enableObscure: true,
                    controller: passwordConfirm,
                    onChanged: onChanged
                ),
                const SizedBox(height: 411),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: Expanded(
                      child: FilledButton(
                          onPressed: (){
                            pressChangePassword(password.text,
                                (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const LogIn()), (route) => false);},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme.of(context).filledButtonTheme.style,
                          child: Text(
                            'Подтвердить',
                            style: Theme.of(context).textTheme.labelMedium,
                          )
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'Я вспомнил свой пароль! ',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                            ),
                            TextSpan(
                                text: 'Вернуться',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
