import 'package:flutter/material.dart';
import 'package:training16/auth/domain/LogInPresenter.dart';
import 'package:training16/auth/presentation/pages/ForgotPassword.dart';
import 'package:training16/common/widgets/CustomTextField.dart';

import '../../../common/colors.dart';
import '../../../common/utils.dart';
import 'Holder.dart';
import 'MainPage.dart';
import 'SignUp.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}
bool isChecked = false;
class _LogInState extends State<LogIn> {
  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    void onChanged(_){}
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Добро пожаловать',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Заполните почту и пароль чтобы продолжить',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Почта",
                    hint: "***********@mail.com",
                    controller: email,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Пароль",
                    hint: "**********",
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                const SizedBox(height: 18),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 22,
                          width: 22,
                          child: Checkbox(
                              value: isChecked,
                              checkColor: colors.background,
                              activeColor: colors.accent,
                              side: BorderSide(color: colors.subtext, width: 1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                              ),
                              onChanged: (value){
                                setState(() {
                                  isChecked = value!;
                                });
                              }
                          ),
                        ),
                        const SizedBox(width: 8),
                        Text(
                            'Запомнить меня',
                          style: Theme.of(context).textTheme.titleMedium,
                        )
                      ],
                    ),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ForgotPassword()));},
                      child: Text(
                        'Забыли пароль?',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 371),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: Expanded(
                      child: FilledButton(
                          onPressed: (){
                            pressSignIn(
                                email.text,
                                password.text,
                                (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const MainPage()), (route) => false);},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme.of(context).filledButtonTheme.style,
                          child: Text(
                            'Войти',
                            style: Theme.of(context).textTheme.labelMedium,
                          )
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));
                  },
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'У меня нет аккаунта! ',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                            ),
                            TextSpan(
                                text: 'Создать',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
