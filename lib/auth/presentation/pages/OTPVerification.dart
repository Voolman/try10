import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:training16/auth/domain/OTPVerificationPresenter.dart';
import 'package:training16/auth/presentation/pages/NewPassword.dart';

import '../../../common/colors.dart';
import '../../../common/utils.dart';
import '../../domain/ForgotPasswordPresenter.dart';
import 'ForgotPassword.dart';
import 'LogIn.dart';

class OTPVerification extends StatefulWidget {
  const OTPVerification({super.key});

  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {
  @override
  Widget build(BuildContext context) {
    TextEditingController code = TextEditingController();
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Верификация',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Введите 6-ти значный код из письма',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 58),
                Pinput(
                  length: 6,
                  controller: code,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  defaultPinTheme: PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                      border: Border.all(color: colors.subtext, width: 1),
                      borderRadius: const BorderRadius.all(Radius.zero)
                    )
                  ),
                  submittedPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          border: Border.all(color: colors.accent, width: 1),
                          borderRadius: const BorderRadius.all(Radius.zero)
                      )
                  ),
                ),
                const SizedBox(height: 48),
                GestureDetector(
                  onTap: (){
                    pressSendOTP(
                        email.text,
                            (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const OTPVerification()), (route) => false);},
                            (String e){showError(context, e);}
                    );
                  },
                  child: Text(
                    'Получить новый код',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                  )
                ),
                const SizedBox(height: 449),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: Expanded(
                      child: FilledButton(
                          onPressed: (){
                            pressVerifyOTP(
                                email.text,
                                code.text,
                                (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const NewPassword()), (route) => false);},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme.of(context).filledButtonTheme.style,
                          child: Text(
                            'Сбросить пароль',
                            style: Theme.of(context).textTheme.labelMedium,
                          )
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'Я вспомнил свой пароль! ',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                            ),
                            TextSpan(
                                text: 'Вернуться',
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                            )
                          ]
                      )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
