import 'package:flutter/material.dart';
import 'package:training16/common/utils.dart';

import '../../../main.dart';
import '../../data/models/PlatformModel.dart';
import '../../domain/ChoosePlatformPresenter.dart';

class ChoosePlatformPage extends StatefulWidget {

  final List<PlatformModel> alreadySelectedPlatforms;

  const ChoosePlatformPage(
      {
        super.key,
        required this.alreadySelectedPlatforms
      }
      );

  @override
  State<ChoosePlatformPage> createState() => _ChoosePlatformPageState();
}

class _ChoosePlatformPageState extends State<ChoosePlatformPage> {

  ChoosePlatformPresenter presenter = ChoosePlatformPresenter();

  @override
  void initState() {
    super.initState();
    presenter.fetchListPlatforms(
        widget.alreadySelectedPlatforms,
            (data){
          setState(() {
            presenter.platforms = data;
          });
        },
    (String e){showError(context, e);}
    );
  }

  List<Widget> getPlatformsWidget(){
    var colors = MyApp.of(context).getColorsApp(context);
    List<Widget> widgets = [];
    for (var model in presenter.platforms.keys){
      bool isSelect = presenter.platforms[model]!;
      widgets.add(
          Column(
            children: [
              Row(
                children: [
                  Image.network(model.getFullIconUrl(), width: 30, height: 30),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Text(model.title, style: TextStyle(
                        color: colors.text,
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                    )),
                  ),
                  GestureDetector(
                      onTap: (){
                        setState(() {
                          if (isSelect){
                            presenter.unselectPlatform(model);
                          }else{
                            presenter.selectPlatform(model);
                          }
                        });
                      },
                      child: Image.asset((isSelect)
                          ? "assets/check.png"
                          : "assets/add.png")
                  )
                ],
              ),
              const SizedBox(height: 28)
            ],
          )
      );
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22),
        child: Column(
          children: <Widget>[
            const SizedBox(height: 67),
            Row(
              children: [
                GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop(presenter.getSelectedPlatform());
                    },
                    child: Icon(Icons.close)
                ),
                const SizedBox(width: 16),
                Text("Выберите платформу", style: TextStyle(
                    color: colors.text,
                    fontSize: 22,
                    fontWeight: FontWeight.w500
                ))
              ],
            ),
            const SizedBox(height: 22),
          ] + getPlatformsWidget(),
        ),
      ),
    );
  }
}
