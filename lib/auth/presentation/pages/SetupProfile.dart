import 'package:flutter/material.dart';
import 'package:training16/auth/presentation/pages/ChoosePlatform.dart';
import 'package:training16/auth/presentation/pages/MainPage.dart';

import '../../../common/utils.dart';
import '../../../common/widgets/CustomTextField.dart';
import '../../../main.dart';
import '../../domain/SetupProfilePresenter.dart';

class SetupProfilePage extends StatefulWidget {
  const SetupProfilePage({super.key});

  @override
  State<SetupProfilePage> createState() => _SetupProfilePageState();
}

class _SetupProfilePageState extends State<SetupProfilePage> {

  var fio = TextEditingController();
  var phone = TextEditingController();

  SetupProfilePresenter presenter = SetupProfilePresenter();


  @override
  void initState() {
    super.initState();
    presenter.fetchUserData((profile) {
      fio.text = profile.fullname;
      phone.text = profile.phone;
      presenter.birthday = profile.birthday;
      setState(() {});
    });
  }

  bool isValid = false;

  void onChanged(_){}

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 73),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)
                          ),
                          minimumSize: Size.zero,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12,
                              horizontal: 12
                          )
                      ),
                      onPressed: (){
                        if (presenter.avatar == null && presenter.avatarUrl == null){
                          showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                title: const Text("Выберите источник"),
                                actions: [
                                  TextButton(onPressed: () async {
                                    await presenter.pressPickAvatarFromCamera(onPick: (){
                                      setState((){});
                                      Navigator.of(context).pop();
                                    });
                                  }, child: const Text("Камера")),
                                  TextButton(onPressed: () async {
                                    await presenter.pressPickAvatarFromGallery(onPick: (){
                                      setState(() {});
                                      Navigator.of(context).pop();
                                    });
                                  }, child: const Text("Галерея")),
                                ],
                              )
                          );
                        }else{
                          setState(() {
                            presenter.deleteAvatar();
                          });
                        }
                      },
                      child: Icon(
                        (presenter.avatar == null && presenter.avatarUrl == null)
                            ? Icons.add_photo_alternate_outlined
                            : Icons.delete_outline,
                        size: 21,
                        color: colors.accent,
                      )
                  ),
                  Container(
                    width: 151,
                    height: 151,
                    decoration: BoxDecoration(
                        color: colors.block,
                        borderRadius: BorderRadius.circular(32)
                    ),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(32),
                        child: (presenter.avatarUrl == null)
                            ? (presenter.avatar == null)
                            ? Center(child: Image.asset("assets/vector.png"))
                            : Image.memory(presenter.avatar!, fit: BoxFit.cover)
                            : Image.network(presenter.avatarUrl!, fit: BoxFit.cover)
                    ),
                  ),
                  OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)
                          ),
                          minimumSize: Size.zero,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12,
                              horizontal: 12
                          )
                      ),
                      onPressed: (){
                        setState(() {
                          presenter.pressChangeTheme(context);
                        });
                      },
                      child: Icon(
                        Icons.sunny,
                        size: 21,
                        color: colors.accent,
                      )
                  ),
                ],
              ),
              CustomTextField(
                label: "ФИО",
                hint: "Введите ваше ФИО",
                controller: fio,
                onChanged: onChanged,
              ),
              CustomTextField(
                label: "Телефон",
                hint: "+7 (000) 000 00 00",
                controller: phone,
                onChanged: onChanged,
              ),
              const SizedBox(height: 24),
              Text(
                "Дата рождения",
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: colors.subtext
                ),
              ),
              const SizedBox(height: 8),
              GestureDetector(
                onTap: () async {
                  var birthday = await showDatePicker(
                    context: context,
                    firstDate: DateTime.utc(0),
                    lastDate: DateTime.now(),
                  );
                  presenter.setFormattedBirthday(birthday!);
                  setState(() {});
                },
                child: Container(
                  height: 44,
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(
                          color: colors.subtext
                      )
                  ),
                  child: Text(
                    (presenter.birthday == null)
                        ? "Выберите дату"
                        : presenter.birthday!,
                    style: TextStyle(
                        color: colors.text,
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 24),
              Text(
                "Источники новостей",
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: colors.subtext
                ),
              ),
              const SizedBox(height: 8),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChoosePlatformPage(alreadySelectedPlatforms: presenter.platforms)));
                },
                child: Container(
                  height: 44,
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(
                          color: colors.subtext
                      )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Выберите платформы",
                        style: TextStyle(
                            color: colors.text,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),
                      ),
                      const Icon(Icons.add, color: Color.fromARGB(100, 32, 32, 32))
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 24),
              SizedBox(
                width: double.infinity,
                child: FilledButton(
                    onPressed: () async {
                      await presenter.pressConfirm(
                          fio.text,
                          phone.text,
                              (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const MainPage()), (_) => false);},
                              (error) => showError(context, error)
                      );
                    },
                    child: Text(
                        (presenter.isAlreadyExistMetadata) ? "Сохранить" : "Продолжить",
                    style: TextStyle(color: colors.background),
                    )
                ),
              ),
              (presenter.isAlreadyExistMetadata)
                  ? Padding(
                padding: const EdgeInsets.only(top: 18),
                child: SizedBox(
                  width: double.infinity,
                  child: OutlinedButton(
                      onPressed: (){
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (_) => const MainPage()),
                                (_) => false
                        );
                      },
                      child: Text("Отменить", style: TextStyle(color: colors.accent))
                  ),
                ),
              )
                  : const SizedBox(height: 0),
              const SizedBox(height: 32)
            ],
          ),
        ),
      ),
    );
  }
}
