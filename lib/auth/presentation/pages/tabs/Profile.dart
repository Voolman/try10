import 'dart:io';

import 'package:flutter/material.dart';
import 'package:training16/auth/presentation/pages/SetupProfile.dart';

import '../../../../common/utils.dart';
import '../../../../main.dart';
import '../../../domain/HolderPresenter.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.only(top:73, left: 24, right: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(32),
                  child: Image.asset('assets/vector.png', height: 121, width: 121),
                ),
                const SizedBox(width: 24),
                Expanded(
                  child: Text(
                    'Смирнов Александр Максимович',
                    style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 18),
                  ),
                )
              ],
            ),
            const SizedBox(height: 28),
            Divider(height: 1, color: colors.subtext,),
            const SizedBox(height: 25),
            Text(
              'История',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 18),
            Text(
              'Прочитанные статьи',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 18),
            Text(
              'Чёрный список',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 24),
            Text(
              'Настройки',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 18, fontWeight: FontWeight.w600),
            ),
            const SizedBox(height: 18),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const SetupProfilePage()), (route) => false);
              },
              child: Text(
                'Редактирование профиля',
                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
              ),
            ),
            const SizedBox(height: 18),
            Text(
              'Политика конфиденциальности',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 18),
            Text(
              'Оффлайн чтение',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 18),
            Text(
              'О нас',
              style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            const SizedBox(height: 24),
            SizedBox(
              height: 46,
              width: double.infinity,
              child: OutlinedButton(
                  onPressed: (){
                    pressSignOut(
                            (){exit(0);},
                            (String e){showError(context, e);}
                    );
                  }, 
                  style: OutlinedButton.styleFrom(
                    side: BorderSide(color: colors.error, width: 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4)
                    )
                  ),
                  child: Text(
                    'Выход',
                    style: TextStyle(
                      color: colors.error,
                      fontSize: 16,
                      fontWeight: FontWeight.w400
                    ),
                  )
              ),
            )
          ],
        ),
      ),
    );
  }
}