import 'package:flutter/material.dart';
import 'package:training16/auth/domain/SignUpPresenter.dart';
import 'package:training16/auth/presentation/pages/Holder.dart';
import 'package:training16/auth/presentation/pages/MainPage.dart';
import 'package:training16/common/widgets/CustomTextField.dart';

import '../../../common/colors.dart';
import '../../../common/utils.dart';
import 'LogIn.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    TextEditingController passwordConfirm = TextEditingController();
    void onChanged(_){}
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: ListView(
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Создать аккаунт',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Завершите регистрацию чтобы начать',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                CustomTextField(
                    label: "Почта",
                    hint: "***********@mail.com",
                    controller: email,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Пароль",
                    hint: "**********",
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Повторите пароль",
                    hint: "**********",
                    enableObscure: true,
                    controller: passwordConfirm,
                    onChanged: onChanged
                ),
                const SizedBox(height: 319),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: Expanded(
                      child: FilledButton(
                          onPressed: (){
                            pressSignUp(
                                email.text,
                                password.text,
                                (){Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const MainPage()), (route) => false);},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme.of(context).filledButtonTheme.style,
                          child: Text(
                            'Зарегистрироваться',
                            style: Theme.of(context).textTheme.labelMedium,
                          )
                      )
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));
                  },
                  child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: 'У меня уже есть аккаунт! ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                            text: 'Войти',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700, color: colors.accent)
                          )
                        ]
                  )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
