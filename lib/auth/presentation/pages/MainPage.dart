import 'package:flutter/material.dart';
import 'package:training16/auth/presentation/pages/tabs/Favorite.dart';
import 'package:training16/auth/presentation/pages/tabs/Home.dart';
import 'package:training16/auth/presentation/pages/tabs/Profile.dart';
import 'package:training16/auth/presentation/pages/tabs/Search.dart';
import 'package:training16/main.dart';


class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}
int currentIndex = 0;
class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
        backgroundColor: colors.background,
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: colors.background,
            currentIndex: currentIndex,
            type: BottomNavigationBarType.fixed,
            selectedLabelStyle: Theme.of(context).bottomNavigationBarTheme.selectedLabelStyle,
            unselectedLabelStyle: Theme.of(context).bottomNavigationBarTheme.unselectedLabelStyle,
            unselectedItemColor: colors.subtext,
            selectedItemColor: colors.accent,
            showUnselectedLabels: true,
            elevation: 0,
            onTap: (newIndex){
              setState(() {
                currentIndex = newIndex;
              });
            },
        items: [
          BottomNavigationBarItem(icon: Icon((currentIndex == 0) ? Icons.home : Icons.home_outlined), label: "Home"),
          const BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
          BottomNavigationBarItem(icon: Icon((currentIndex == 2) ? Icons.bookmark : Icons.bookmark_outline), label: "Favorite"),
          BottomNavigationBarItem(icon: (currentIndex == 3) ? Image.asset("assets/profile.png") : Image.asset("assets/unsel_profile.png"), label: 'Profile')
        ]
    ),
      body: [const Home(), const Search(), const Favorite(), const Profile()][currentIndex],
    );
  }
}
