import 'dart:typed_data';

import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training16/auth/data/models/PlatformModel.dart';

import '../models/ProfileModel.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return await supabase.auth.signUp(email: email, password: password);
}

Future<AuthResponse> signIn(String email, String password) async {
  return await supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return await supabase.auth.signOut();
}


Future<void> sendOTP(String email) async {
  return await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) async {
  return await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) async {
  return await supabase.auth.updateUser(UserAttributes(password: password));
}

List<PlatformModel> loadPlatform(){
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!['platforms'];
  return shortData.map((e) => PlatformModel.fromJson(e)).toList();
}

Future<PlatformModel> getPlatforms(String id) async {
  var response = await supabase.from('platforms').select().eq("id", id).single();
  return PlatformModel.fromJson(response);
}

Future<List<PlatformModel>> getAllPlatform() async {
  var response = await supabase.from('platforms').select();
  return response.map((Map<String, dynamic> e) => PlatformModel.fromJson(e)).toList();
}

Future<UserResponse> savePlatforms(List<PlatformModel> platforms) async {
  return await supabase.auth.updateUser(UserAttributes(data: {"platforms": platforms.map((e) => e.toJson()).toList()}));
}

ModelProfile getModelProfile() {
  var userMetadata = supabase.auth.currentUser?.userMetadata!;
  var platforms = loadPlatform();
  return ModelProfile(
      fullname: userMetadata?["fullname"],
      phone: userMetadata?["phone"],
      birthday: userMetadata?["birthday"],
      avatar: userMetadata?["avatar"],
      platforms: platforms
  );
}

bool checkExistPlatformsMetadata(){
  return supabase.auth.currentUser?.userMetadata?.containsKey("platforms") ?? false;
}

Future<void> saveUserData(
    String fullname,
    String phone,
    String birthday
    ) async {
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "fullname": fullname,
            "phone": phone,
            "birthday": birthday,
          }
      )
  );
}

Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  await supabase.storage
      .from("avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": name
          }
      )
  );
}

Future<void> removeAvatar() async {
  await supabase.storage
      .from("avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": null
          }
      )
  );
}

