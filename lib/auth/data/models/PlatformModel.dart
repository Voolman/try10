class PlatformModel{
  final String id;
  final String icon;
  final String title;
  final String defaultChannels;
  final String availableChannels;
  String channels;

  PlatformModel({
    required this.id,
    required this.icon,
    required this.title,
    required this.defaultChannels,
    required this.availableChannels,
  }): channels = defaultChannels;

  String getFullIconUrl(){
    return '$icon';
  }

  static PlatformModel fromJson(Map<String, dynamic> json){
    return PlatformModel(
        id: json["id"],
        icon: json["icon"],
        title: json["title"],
        defaultChannels: json["default_channels"],
        availableChannels: json["allow_channels"]
    );
  }

  Map<String, dynamic> toJson(){
    return {
      'id': id,
      "icon": icon,
      'title': title,
      "defaultChannels": defaultChannels,
      "allow_channels": availableChannels
    };
  }
}